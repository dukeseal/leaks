# Hunting Memory Leaks in JavaFX WebView #

The goal of this repository is to track down problems with JavaFX WebView, knockout.js and DukeScript. Let's start with plain knockout.js application, measure its memory behavior and then slowly modify it to use more and more Java features.

### How run the Application? ###

```
$ git clone https://dukeseal@bitbucket.org/dukeseal/leaks.git
$ cd leaks
$ mvn install
$ JAVA_HOME=/path/jdk1.8.0_112 mvn -f client/pom.xml exec:exec
```

A webview window opens up. Click the button few times to let the timers start counting.

### Leaking Strings

The change [3e5d362fe](https://bitbucket.org/dukeseal/leaks/commits/3e5d362fe4f2f6e495f1e6ff663d9b407a774972) shows the leak. If the string is set in JavaScript, everything is fine. If it is set by Java, the leak appears. Please verify.

The next step is to report bug to JavaFX team, I guess.

### Callbacks from Java to JavaScript don't leak

Version [dbfb62a35](https://bitbucket.org/dukeseal/leaks/commits/dbfb62a35ec6189577514fb6ebf5f55c50f9ebe9) replaces direct use of `window.setTimeout` with `java.util.Timer` - each two seconds a callback from Java to JavaScript is made. There don't seem to be any leaks so far:

```bash
6907812 255104  69884 S  3,2   0:41.28 java
# Test case executed in 1398 times!
7087064 369668  70388 S  4,6  27:23.36 java
# 65380 times
7087064 368472  70388 S  4,6  48:01.36 java 
# Test case executed in 109391 times!
7087064 368472  70388 S  4,6  48:01.36 java
# Test case executed in 431047 times!
```
The occupied memory seems stable for three hundred thousand of invocations.

### Current Results for JavaScript only version

Most [recent version](https://bitbucket.org/dukeseal/leaks/commits/8921d9de5e9e925b8922dec8cad15fb5abba6907?at=master) 
running on top of `JDK1.8.0_112` seems to be stable enough:

```bash
5756416 485916  71308 S  6,0   6:33.29 java
5756420 513124  71308 S  6,4  32:33.28 java
5756424 470732  71308 S  5,8  82:34.19 java 
# Test case executed in 224016 times!^C
5756428 471492  71628 S  5,8 107:05.86 java 
# Test case executed in 289789 times!
```

After running for more than eight hours the memory seems to remain stable. Time to turn parts of the application into
**Java** ones.

### Initial (and wrong) Measurement

Following results were observed using `top` on `JDK 1.8.0 u101` and [initial changeset](https://bitbucket.org/dukeseal/leaks/commits/86b62be16390c20a6931253bae563bd7eac64ec8):

```
6989004 221644  73356 S   7,0  2,7   0:05.49 java  
7126956 307452  23648 S  24,0  3,8  47:40.98 java 
7151880 325864  25536 S  26,6  4,0  57:55.41 java      
7177672 348252  24916 S  15,3  4,3 171:14.80 java
```

The application was running for two days and there was slight increase in memory consumption. At the end the application was displaying:
```
current array size is 1000
Test case executed in 130053 times!
```

The results might have been influenced by NetBeans inspector - better to [disable it](https://bitbucket.org/dukeseal/leaks/commits/8921d9de5e9e925b8922dec8cad15fb5abba6907). Now the measurements are running again.