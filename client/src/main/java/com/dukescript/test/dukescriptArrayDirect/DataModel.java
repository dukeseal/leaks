package com.dukescript.test.dukescriptArrayDirect;

import com.dukescript.test.dukescriptArrayDirect.js.Dialogs;
import net.java.html.json.Model;

@Model(className = "Data", targetId="", properties = {
})
final class DataModel {
    private static Data ui;
    /**
     * Called when the page is ready.
     */
    static void onPageLoad() throws Exception {
        ui = new Data();
        Dialogs.registerSchedule();
        Dialogs.registerSubViewModel();
      //  ui.applyBindings();
    }
}
