// Here's my data model

function wrap(initialValue) {
    var value = initialValue;
    function f() {
      if (arguments.length === 1) {
          value = arguments[0];
          updateKoData();
      }
      return value;
    }
    f.removeAll = function() {
        while (value.length > 0) {
            value.pop();
        }
    }
    f.push = function(x) {
        var r = value.push(x);
        f.length = value.length;
        updateKoData();
        return r;
    }
    return f;
}

function identity(f) {
    return f;
}

var ko = {
    'observable' : wrap,
    'observableArray' : wrap,
    'computed' : identity,
    'applyBindings' : function(data) {
        ko.data = data;
        updateKoData();
    }
};

function updateKoData() {
    for (var p in ko.data) {
        var e = document.getElementById(p);
        if (e) {
            e.innerHTML = ko.data[p]();
        }
    }
}

if (!window.schedule) {
    window.schedule = window.setInterval;
}

function SubViewModel(number) {
    var self = this;
    if (typeof SubViewModelJava == 'function') {
        if (SubViewModelJava(self, number)) {
            return;
        }
    }
    self.num = number;
    self.name = "anything";
    self.content = "have a long string to take more memory .............................take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory ." + number;
}


var ViewModel = function (first, last) {
    var self = this;
    self.firstName = ko.observable(first);
    self.lastName = ko.observable(last);
    self.numberOfExecution = ko.observable(0);

    self.fullName = ko.computed(function () {
        // Knockout tracks dependencies automatically. It knows that fullName depends on firstName and lastName, because these get called when evaluating fullName.
        return this.firstName() + " " + this.lastName();
    }, this);

    self.testArray = ko.observableArray([])

    self.testLength = ko.computed(function () {
        return self.testArray().length;
    }, this);

    doReplaceArray = function () {
        self.testArray.removeAll();
        for (i = 0; i < 1000; i++) {
            self.testArray.push(new SubViewModel(i));
        }
        self.numberOfExecution(self.numberOfExecution() + 1);
    };

    self.replaceArray = function() {
        window.schedule(doReplaceArray, 2000);
    };

};

ko.applyBindings(new ViewModel("Planet", "Earth")); // This makes Knockout get to work
