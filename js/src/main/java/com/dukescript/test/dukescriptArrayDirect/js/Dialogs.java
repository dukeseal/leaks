package com.dukescript.test.dukescriptArrayDirect.js;

import java.util.Timer;
import java.util.TimerTask;
import net.java.html.BrwsrCtx;
import net.java.html.js.JavaScriptBody;
import netscape.javascript.JSObject;

/** Use {@link JavaScriptBody} annotation on methods to
 * directly interact with JavaScript. See
 * http://bits.netbeans.org/html+java/1.2/net/java/html/js/package-summary.html
 * to understand how.
 */
public final class Dialogs {
    private static final boolean INIT_IN_JS = Boolean.getBoolean("init.in.js");
    private static final Timer TIMER = new Timer("Java Timer");
    private static BrwsrCtx CTX;

    private Dialogs() {
    }

    public static void registerSchedule() {
        CTX = BrwsrCtx.findDefault(Dialogs.class);
        registerTimer0();
    }

    @JavaScriptBody(args = {}, javacall = true, body = "\n"
        + "window.schedule = function(f,delay) {\n"
        + "  @com.dukescript.test.dukescriptArrayDirect.js.Dialogs::schedule(Ljava/lang/Object;I)(f, delay);\n"
        + "}\n")
    private static native void registerTimer0();

    public static void registerSubViewModel() {
        if (!INIT_IN_JS) {
            registerSubViewModel0();
        }
    }
    
    @JavaScriptBody(args = {}, javacall = true, body = "\n"
        + "window.SubViewModelJava = function(thiz, num) {\n"
        + "  return @com.dukescript.test.dukescriptArrayDirect.js.Dialogs::initModel(Ljava/lang/Object;I)(thiz, num);\n"
        + "}\n")
    private static native void registerSubViewModel0();
    
    static boolean initModel(Object thiz, int number) {
        if (thiz instanceof JSObject) {
            JSObject js = (JSObject) thiz;
            if (!INIT_IN_JS) {
                js.setMember("num", number);
                js.setMember("name", "anything");
                js.setMember("content", "have a long string to take more memory .............................take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory ." + number);
                return true;
            }
        } else {
            if (!INIT_IN_JS) {
                setMember(thiz, "num", number);
                setMember(thiz, "name", "anything");
                setMember(thiz, "content", "have a long string to take more memory .............................take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory take more memory ." + number);
                return true;
            }
        }
        return false;
    }
    
    @JavaScriptBody(args = { "jsObj", "property", "value" }, body = 
        "jsObj[property] = value;\n"
    )
    private static native void setMember(Object jsObj, String property, Object value);

    static void schedule(final Object function, int delay) {
        TIMER.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                CTX.execute(new Runnable() {
                    @Override
                    public void run() {
                        invokeFunction(function);
                    }
                });
            }

        }, delay, delay);
    }

    @JavaScriptBody(args = { "fn" }, body = "fn()")
    static native void invokeFunction(Object fn);
}
